﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        public static void Main(string[] args)
        {

            int numb;
            int fact = 1;
            Console.WriteLine("A PROGRAM TO FIND THE FACTORIAL OF ANY NUMBER!");

            Console.WriteLine("Enter the number");
            numb = Convert.ToInt32(Console.ReadLine());

            
            for (int i = 1 ; i <= numb; i++)
            {
                fact = fact * i;
            }

            Console.WriteLine("\n");
            Console.WriteLine(fact);
            Console.WriteLine("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}
